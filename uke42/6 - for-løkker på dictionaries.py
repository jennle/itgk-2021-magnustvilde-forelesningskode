'''for-løkker på dictionaries'''

# en dictionary der nøkkel er satt til "ticker",
# verdi er satt til pris per aksje 
# EQNR er Equinor, DNB er DNB Bank ASA,
# NHY er Norsk Hydro, PMG er Play Magnus
aksjekurs_18okt = {'EQNR': 233.90, 'DNB': 210.50,
                   'NHY': 71.04, 'PMG': 18.52}

# skriv ut alle tickere i dictionaryen med en for-løkke
for key in aksjekurs_18okt:
    print(key)
print()

# skriv ut en oversikt over hvilken nøkkel som peker på hvilken verdi,
# bruk for-løkke
for key in aksjekurs_18okt.keys():
    print(key, ': ', str(aksjekurs_18okt[key]))
