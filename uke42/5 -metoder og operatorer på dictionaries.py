'''Metoder og operatorer på dictionaries'''

person = {'name': 'Alex', 'age': 17, 'drivers license': False,
          'height': 175, 'sex': 'non-binary'}

# fjern nøkkelen "sex" med metoden pop()
person.pop('sex')
print(person)

# sjekk om nøkkelen height finnes i nøklene til person
print('height' in person)

# skriv ut alle verdier til dictionaryen med metoden values()
print(person.values())

# skriv ut alle verdier til dictionaryen med metoden keys()
print(person.keys())

