'''if-elif-else'''

# Godteri koster 30, og brus koster 20.
# Skriv ut hva bruker kan kjøpe.
penger = 30

godteri = 30
brus = 20

if (penger >= godteri + brus):
    print('Du kan kjøpe både godteri og brus!')
elif (penger >= godteri):
    print('Du kan kjøpe godteri!')
elif (penger >= brus):
    print('Du kan kjøpe brus!')
else:
    print('Du har ikke så mye penger, dessverre.')
