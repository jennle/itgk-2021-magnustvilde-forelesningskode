'''fra struktur i kode til fil - to-dimensjonal liste'''

personer = [['Ola', 45], ['Kari', 23], ['Hermann', 87],
            ['Mikkel', 34], ['Nora', 16], ['Harald', 37],
            ['Anna', 65], ['Thea', 39]]

# Skriv listen til en tekstfil, på følgende format:
'''
navn, alder
navn, alder
...
'''
