'''Mer om strenger'''

# Hvordan kan vi få tak i en bokstav?
tekst = 'Dette er min streng!'
a = tekst[0]
print(a)

# Få tak i siste bokstav
b= tekst[-1]
print(b)

# legge sammen strenger, gange strenger
leggeSammen = a+b
print(leggeSammen)

leggeSammen *= 3
print(leggeSammen)

# legge sammen strenger og andre datatyper
tall = 5
flyt = 4.7

datatyper = tekst + str(tall) + str(flyt)
print(datatyper)

kjappere = f'{tekst}{tall}{flyt}'
print(kjappere)

# hent ut annenhver bokstav i en streng, og lagre det i en variabel
kortStreng = 'Magnus'
annenHver = kortStreng[0] + kortStreng[2] + kortStreng[4]
print(annenHver)

