'''binærfiler'''

tickere = {'PMG': 'Play Magnus', 'EQNR': 'Equinor',
           'DNB': 'DNB Bank ASA', 'NHY': 'Norsk Hydro'}

# hent modulen 'pickle' slik at vi kan gjøre ting med binærfiler
import pickle

# åpne filen '1_tics.dat' med tilgangstypen 'wb' (write binary)
# skriv dictionaryen vår 'tickere' til filen
with open('1_tics.dat', 'wb') as fil:
    pickle.dump(tickere, fil)

# lag en funksjon som leser en binærfil. Ta inn en parameter
# for filnavnet. Bruk tilgangstypen 'rb' (read binary).
# Returner det vi leser fra filen.
def les_bin(filnavn):
    with open(filnavn, 'rb') as fil_les:
        dic = pickle.load(fil_les)
    return dic

# kall på funksjonen og lagre returverdien i en variabel
# husk  å bruke riktig filnavn
innhold = les_bin('1_tics.dat')
