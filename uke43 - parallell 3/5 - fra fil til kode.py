'''fra tekstfil til struktur i kode'''

# skriv innholdet i '5_aksjer.txt' til en dictionary
# på formen {ticker: [pris_aksje, navn_aksje]}
# pris_aksje skal være float
aksjer = {}
# Åpne fil med mulighet for å lese
with open('5_aksjer.txt', 'r') as fil:
    # Les innholdet inn i en liste
    linjer = fil.readlines()
    for linje in linjer:
        linje = linje.strip('\n')
        innhold = linje.split(', ')
        innhold[1] = float(innhold[1])
        nokkel, pris, navn = innhold[0], innhold[1], innhold[2]
        aksjer[nokkel] = [pris, navn]
        print(aksjer)

print(aksjer)


