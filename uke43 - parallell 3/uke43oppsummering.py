'''Oppsummering uke 43'''

# åpne filer:
def apne_fil(filnavn, tilgangstype):
    with open(filnavn, tilgangstype) as fil:
        pass

# importere pickle for å kunne håndtere binærfiler
import pickle
# skrive til binærfil
with open('min_binærfil.dat', 'wb') as fil:
    pickle.dump(['Liste', 'av', 'innhold'], fil)

# lese fra binærfil
with open('min_binærfil.dat', 'rb') as fil:
    innhold = pickle.load(fil)

#skrive til tekstfil med 'w' (write, overskriving)
with ('tekstfil.txt', 'w') as fil:
    fil.write('Tekst')
    fil.writelines(['Tekst', 'Ny linje', 'Enda en linje'])

# skrive til tekstfil med 'a' (append, legge til)
with ('tekstfil.txt', 'a') as fil:
    fil.write('Legges på slutten')
    fil.writelines(['Legges', 'forsatt', 'til sist'])


# lese fra tekstfil
with ('tekstfil.txt', 'r') as fil:
    hele = fil.read()
    #NB! De under fungerer ikke HER fordi vi har allerede lest hele filen
    # vanligvis fungerer de, men det er viktig å ta hensyn til programpekeren
    linjer = fil.readlines()
    en_linje = fil.readline()
    
# exceptions, generell except
try:
    #kode som vi prøver å kjøre
    pass
except:
    # kode som kjøres dersom det skjer en feil i 'try'-blokken
    pass

#spesifiserte exceptions
try:
    svar = float(input('Tall: '))
except ValueError:
    #kjøres dersom feil input
    print('Feil format.')
# else og finally i strukturen
else:
    #kjøres dersom except ikke kjøres
    print('Riktig format.')
finally:
    # kjøres uansett til slutt
    print('Programmet er ferdig.')