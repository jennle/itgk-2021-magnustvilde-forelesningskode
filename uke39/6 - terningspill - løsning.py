'''funksjon med returverdi og random-modulen'''

#Importer random-modulen (du trenger kun randint)
from random import randint

#Ta inn navnet til en bruker i en funksjon. Returner navnet
def hent_navn():
    return input('Navn på spiller: ')

#Bruk funksjonen til å lage to brukere
spiller1, spiller2 = hent_navn(), hent_navn()

#Lag en funksjon som heter trill_terninger
    #Print ut info om spillet, samt hvordan man avslutter
    #De to brukerene skal trille terninger om og om igjen så lenge brukeren vil
    #Hold styr på hvor mange poeng de har
    #Gi ett poeng til den som får høyest terningkast, null dersom ulik
    #Returner hvor mange poeng spillerene har etter funksjonen er ferdig.
def trill_terning():
    print('Terningspill! Skriv "N" for å avslutte.')
    print('Poeng blir gitt til spilleren som triller høyest.')
    spille = input('Enter for å trille, N for å avslutte:  ')
    poeng1, poeng2 = 0, 0
    while spille.lower() != 'n':
        trill1, trill2 = randint(1,6), randint(1,6)
        print(f'{spiller1}: {trill1}\n{spiller2}: {trill2}')
        if (trill1 > trill2):
            poeng1+=1
            print(f'Poeng til {spiller1}.')
        elif(trill1 < trill2):
            poeng2+=1
            print(f'Poeng til {spiller2}.')
        else:
            print('Ingen poeng.')
        spille = input('Enter for å trille, N for å avslutte: ')
    return poeng1, poeng2


# Lag en funksjon som tar inn to parametre.
# Funksjonen skal skrive ut poengscore og hvem som vinner.
def finn_vinner(en, to):
    print('Spillet er avsluttet!')
    print('Poengfordeling:')
    print(f'{spiller1}\t-\t{en}')
    print(f'{spiller2}\t-\t{to}')
    if en > to:
        return f'Vinneren er {spiller1}.'
    elif to > en:
        return f'Vinneren er {spiller2}.'
    else:
        return f'Begge spillere har {en} poeng, ingen vinner.'
    
score1, score2 = trill_terning()
print(finn_vinner(score1, score2))