'''Moduler og import'''

# importer modulen math og regn ut kvadratroten av et tall
import math
print(math.sqrt(75))


# importer modulen random og lag til tre tilfeldige flyttall
import random
a, b, c = random.random(), random.random(), random.random()
print(f'a: {a}, b: {b}, c: {c}')


vokaler = 'aeiouyæøå'
konsonanter = 'bcdfghjklmnpqrstvwxz'
# Lag en funksjon 'ordgenerator' som tar inn en parameter lengde. Returner
# et ord som begynner på en tilfeldig konsonant, og har vokal som annenhver
# bokstav
from random import randint as r

def ordgenerator(lengde):
    streng =''
    for i in range(lengde):
        if i%2== 0:
            streng += konsonanter[r(0,len(konsonanter)-1)]
        else:
            streng += vokaler[r(0,len(vokaler)-1)]
    return streng

ord1 = ordgenerator(4)
ord2 = ordgenerator(4)

#skriv  ut resultatet av ordgenerator(4)