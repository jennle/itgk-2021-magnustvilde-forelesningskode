'''Funksjoner med returverdi'''

# Lag en funksjon som spør om fornavn, og så etternavn
# returner navnet
def sett_navn():
    fornavn = input('Fornavn: ')
    etternavn = input('Etternavn: ')
    return fornavn + ' ' + etternavn
    
navn = sett_navn()

# Lag en funksjon som tar inn navn som parameter
# spør navn om alder og bosted
# returner alder og bosted

def alder_bosted(ditt_navn):
    alder = int(input(f'{ditt_navn} sin alder: '))
    bosted = input(f'{ditt_navn} sitt bosted: ')
    return alder, bosted
    
    
a, b = alder_bosted(navn)
