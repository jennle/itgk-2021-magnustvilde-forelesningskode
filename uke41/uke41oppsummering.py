'''
Oppsummering uke 41
'''

liste = [1,2,3]
# metoder på lister
liste.sort() #endrer direkte på listen

# funksjoner på lister
liste = sorted(liste) #returnerer en liste som er sortert

# operatorer på lister
a = 1 in liste #gir True

# to-dimensjonale lister
verden = [['Norge', 'Latvia', 'Spania'],['USA', 'Canada','Mexico'],
          ['Kina', 'India', 'Pakistan'],['Kongo', 'Egypt', 'Namibia']]

# indeksering av to-dimensjonale lister
norge = verden[0][0]
asia = verden[2]
canada = verden[1][1]

# for-løkke på to-dimensjonale lister
for kontinent in verden:
    print(kontinent)

# nøstet for-løkke på to-dimensjonale lister
for kontinent in verden:
    for land in kontinent:
        print(land)

###
streng = 'En streng, med komma'
# metoder, funksjoner og operatorer på strenger
streng = streng.replace(',','.')
lengde = len(streng)
delt_opp = streng.split(' ')

# slicing av strenger
forste_tre = streng[:3]
tre_og_ut = streng[3:]
annenhver = streng[::2]
legge_til = 'PLUSS'
legge_til_streng = streng[:3] + legge_til + streng[3:]

# for-løkke på streng, direkte
for tegn in streng:
    print(tegn)

# for-løkke på streng, via indeks
for i in range len(streng):
    print(streng[i])

