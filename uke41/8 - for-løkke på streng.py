'''for-løkke på streng'''

# lag en streng
min_streng = 'Dette er min streng!!! Hei, hei'

# iterer over strengen direkte på tegn
def iterer_streng(string):
    for tegn in string:
        print(tegn)

# iterer_streng(min_streng)

# iterer over strengen via indeks
def iterer_indeks(string):
    for i in range(len(string)):
        print(string[i:])

# iterer_indeks(min_streng)

min_streng = 'Dette er min streng!!! Hei, hei'
# bytt ut alle vokaler i strengen med '@'
#og print ut resultatet
def legg_inn_alfa(string):
    vokaler = 'aeiouyøæå'
    for i in range(len(string)):
        if string[i].lower() in vokaler:
            string = string[:i] + '@' + string[i+1:]
    print(string)

legg_inn_alfa(min_streng)
