'''to-dimensjonale lister'''

# lag en to-dimensjonal liste der hver av de innerste
# listene inneholder noen land fra ett kontinent
verden = [['Norge', 'Latvia', 'Spania'],['USA', 'Canada','Mexico'],
          ['Kina', 'India', 'Pakistan'],['Kongo', 'Egypt', 'Namibia']]

# skriv ut det andre landet fra første kontinent
print(verden[0])
print(verden[0][1])

# skriv ut det første landet fra det andre kontinentet
print(verden[1])
print(verden[1][0])
print()
# skriv ut ett land fra hvert kontinent
for kontinent in verden:
    for land in kontinent:
        print(land)
