'''metoder på lister'''

# lag en liste med en tilfeldig tall-sekvens.
liste = [3,67,2,1,8,90]
#Lag en ny liste som
# inneholder alle elementene til den første listen i sortert rekkefølge
# bruk sort()-metoden
liste.sort()
print(liste)
ny = liste.copy()
print(ny)

# legg til elementer i listen med metodene append() og insert()
liste.append('Hei')
liste.insert(4,'Neida')
print(liste)

# reverser rekkefølgen på listen vha. en metode
liste.reverse()
print(liste)

