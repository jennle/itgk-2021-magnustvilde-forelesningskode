'''funksjoner på lister'''

min_liste = [4,6,7,2,7,9]
# sorter min_liste med sorted()-funksjonen
min_liste = sorted(min_liste)
print(min_liste)

# finn summen av listen med sum()-funksjonen
total = sum(min_liste)
print(total)
